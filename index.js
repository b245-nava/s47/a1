// console.log('JS DOM - Manipulation');

// [Section] Document Object Model
	// Allows us to access or modify the properties of an HTML element in a webpage.
	// A standard on how to get, change, add, or delete HTML elements.
	// For now, we will be focusing on how to manage forms with DOM.

	// For selecting HTML elements, we will be using document.querySelector() / getElementById
	/*
		Syntax:
			document.querySelector("html element")
	*/

	// CSS Selectors
	/*
		- class selector (.)
		- id selector (#)
		- tag selector (html tags)
		- universal selector (*)
		- attribute selector ([attribute])
	*/

// querySelectorAll
let universalSelector = document.querySelectorAll("*");

// querySelector
let singleUniversalSelector = document.querySelector("*");

console.log(universalSelector);
console.log(singleUniversalSelector);

let classSelector = document.querySelectorAll(".full-name");
console.log(classSelector);
// This returns 2 elements within an array.

let singleClassSelector = document.querySelector(".full-name");
console.log(singleClassSelector);
// This returns 1 item, <input type="text" id = "txt-first-name" class = "full-name">.

// document.querySelector targets the very first element that matches its criteria, while .querySelectorAll targets all elements that fit the criteria
// Remember, JS reads scripts from top to bottom, left to right.

let idSelector = document.querySelector('#txt-first-name');
console.log(idSelector);

// Recall that #id is a unique identifier, so it doesn't really make sense to use querySelectorAll when targeting elements by their #id.

let tagSelector = document.querySelectorAll("input");
console.log(tagSelector);

	// Targeting <span id = "fullName"></span>

		// let spanSelector = document.querySelectorAll('span');
		let spanSelector = document.querySelector('span[id]');
		console.log(spanSelector);

		let spanElems = document.querySelectorAll('span[id]');
		console.log(spanElems);

		// getElement
		let element = document.getElementById('fullName');
		console.log(element)

element = document.getElementsByClassName('full-name')
console.log(element)

// [Section] Event Listeners
	// Whenever a user interacts with a web page, this action is considered as an event.
	// Working with events is a large part of creating interactivity in a web page.
	// Specific functions will be triggered if the event happens.
	// The function used is "addEventListener" and needs two arguments.
		// First argument: string identifying event
		// Second argument: function that listener will trigger once the specified event occurs.


	let txtFirstName = document.querySelector('#txt-first-name');

	// Add event listener

	txtFirstName.addEventListener("keyup", () => {
		// console.log(txtFirstName.value);
		// .value is used to target the content of forms, while .innerHTML allows you to place code and text in between HTML tags
		// .value : form content :: .innerHTML : content between HTML tags
		spanSelector.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
	});

	let txtLastName = document.querySelector('#txt-last-name');

	txtLastName.addEventListener("keyup", () => {

		spanSelector.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
	});


// Activity 1: Changing Text Color
let colorPicker = document.querySelector('#text-color');
console.log(colorPicker);

colorPicker.addEventListener('change', () => {

	color = colorPicker.value
	spanSelector.style.color = `${color}`
	// console.log(color);
});